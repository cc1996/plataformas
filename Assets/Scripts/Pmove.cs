﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pmove : MonoBehaviour {

	public float velx;

	private Vector3 iniPos;
	private float movement;
	private Vector3 tmpPos;

	public float maxx;

	void Awake (){
		

	}	
	void Update () {

		transform.Translate(movement * velx * Time.deltaTime, 0, 0);

		if (transform.position.x >= maxx) {
			tmpPos= new Vector3(maxx,transform.position.y,transform.position.z);
			transform.position = tmpPos;
		}else if (transform.position.x <= - maxx){ 
			tmpPos= new Vector3(-maxx,transform.position.y,transform.position.z);
			transform.position = tmpPos;
		}
	}

}
