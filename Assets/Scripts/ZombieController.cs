﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour
{
    // Variables publicas para poder modifircar desde el inspector
    public float maxS = 11f;

    //Variables privadas
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    private Animator anim;
    private bool flipped = false;
	public bool muerto = false;
	//public class Vector3 = origen;
    // Use this for initialization
    void Awake()
    {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();

        // Obtenemos el Animator Controller para poder modificar sus variables
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "muerte") {
			muerto = true;
			anim.SetBool ("dead", true);
		}
	}
		//void Restart(){
			//this.transform.position = origen;
			//anim.SetBool ("dead", false);
			//muerto = false;
			//anim.Play ("idle");
		//}


	void FixedUpdate()
    {

        //Miramos el input Horizontal
        move = Input.GetAxis("Horizontal");
        rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);

        //Miramos si nos estamos moviendo.
        // OJO!! Nunca comparar con 0 floats, nunca será 0 perfecto, siempre hay un error de redondeo
        if(rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f)
        {
            if((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))
            {
                flipped = !flipped;
                this.transform.rotation = Quaternion.Euler(0, flipped ? 180 : 0, 0);
            }
            anim.SetBool("walk", true);
        }
        else
        {
            anim.SetBool("walk", false);
        }
      
        
        if(Input.GetButtonDown("Jump"))
        {
            rb2d.AddForce(new Vector2(0, 500));
				anim.SetBool("jump", true);


        }
        

    }
}